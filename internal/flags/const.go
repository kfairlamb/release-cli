package flags

const (
	// ServerURL available in the CLI context
	ServerURL = "server-url"
	// JobToken available in the CLI context
	JobToken = "job-token"
	// ProjectID available in the CLI context
	ProjectID = "project-id"
	// Timeout available in the CLI context
	Timeout = "timeout"
	// Name available in the CLI context
	Name = "name"
	// Description available in the CLI context
	Description = "description"
	// TagName available in the CLI context
	TagName = "tag-name"
	// Ref available in the CLI context
	Ref = "ref"
	// AssetsLinksName available in the CLI context
	AssetsLinksName = "assets-links-name"
	// AssetsLinksURL available in the CLI context
	AssetsLinksURL = "assets-links-url"
	// AssetsLink available in the CLI context
	AssetsLink = "assets-link"
	// Milestone available in the CLI context
	Milestone = "milestone"
	// ReleasedAt available in the CLI context
	ReleasedAt = "released-at"
	//enable setting useage of self signed cert
	InsecureHttps = "insecure-https"
)
